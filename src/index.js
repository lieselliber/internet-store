import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from "react-redux";

// import 'semantic-ui-css/semantic.min.css';
import './app.css';

import App from './App';

import createStore from './store';
const store = createStore()


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
